import React, { Component } from 'react';
import './App.css';
import urlFor from './helpers/urlFor';
import axios from 'axios';

// Components
import Nav from './components/Nav';
import List from './components/List';
import Note from './components/Note';
import Flash from './components/Flash';

class App extends Component {
  constructor() {
    super();
    this.state = {
      notes: [],
      note: {},
      showNote: false,
      newTag: false
    };
  }

  toggleNote = () => {
    this.setState({
      showNote: !this.state.showNote,
      note: {},
      newTag: false
    });
  }

  getNotes = () => {
    axios.get(urlFor('notes'))
    .then((res) => this.setState({ notes: res.data }) )
    .catch((err) => console.log(err.response.data) );
  }

  getNote = (id) => {
    axios.get(urlFor(`notes/${id}`))
    .then((res) => this.setState({ note: res.data, showNote: true }) )
    .catch((err) => console.log(err.response.data) );
  }

  performSubmissionRequest = (data, id) => {
    if (id) {
      return axios.patch(urlFor(`notes/${id}`), data);
    } else {
      return axios.post(urlFor('notes'), data);
    }
  }

  submitNote = (data, id) => {
    this.performSubmissionRequest(data, id)
    .then((res) => this.setState({ showNote: false }) )
    .catch((err) => {
      const { errors } = err.response.data;
      if (errors.content) {
        this.setState({ error: "Missing Note Content!" });
      } else if (errors.title) {
        this.setState({ error: "Missing Note Title!" });
      }
    });
  }

  deleteNote = (id, index) => {
    const newNotesState = this.state.notes.filter((_, i) => i !== index );
    axios.delete(urlFor(`notes/${id}`))
    .then((res) => this.setState({ notes: newNotesState }))
    .catch((err) => console.log(err.response.data) );
  }

  resetError = () => {
    this.setState({ error: '' });
  }

  // Tag Methods
  showTagForm = () => {
    this.setState({ newTag: true });
  }

  closeTagForm = () => {
    this.setState({ newTag: false });
  }

  submitTag = (data, noteId) => {
    axios.post(urlFor(`notes/${noteId}/tags`), data)
    .then((res) => this.getNote(noteId) )
    .catch((err) => {
      const { errors } = err.response.data;
      if (errors.name) {
        this.setState({ error: "Missing Tag Name!"});
      }
    });
    this.closeTagForm();
  }

  deleteTag  = (noteId, id) => {
    axios.delete(urlFor(`/tags/${id}`))
    .then((res) => this.getNote(noteId) )
    .catch((err) => console.log(err.response.data) );
  }

  render() {
    const { notes, note, showNote, newTag, error } = this.state;

    return (
      <div className="App">
        <Nav
          toggleNote={this.toggleNote}
          showNote={showNote}
        />
        {error &&
          <Flash error={error} resetError={this.resetError}/>
        }
        {showNote ?
          <Note
            note={note}
            submitNote={this.submitNote}
            submitTag={this.submitTag}
            deleteTag={this.deleteTag}
            newTag={newTag}
            showTagForm={this.showTagForm}
            closeTagForm={this.closeTagForm}
          />
          :
          <List
            notes={notes}
            getNotes={this.getNotes}
            getNote={this.getNote}
            deleteNote={this.deleteNote}
          />
        }
      </div>
    );
  }
}

export default App;
