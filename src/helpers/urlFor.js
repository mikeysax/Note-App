const urlFor = (path) => {
  return 'https://firehose-note-api.herokuapp.com/' + path;
};

export default urlFor;
