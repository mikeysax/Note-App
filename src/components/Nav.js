import React, { Component } from 'react';

class Nav extends Component {
  render() {
    const { showNote, toggleNote } = this.props;

    return (
      <div className="nav-container">
        <div className="nav-logo">Note</div>
        <div className="nav-button" onClick={(e) => toggleNote(e)}>
          {showNote ? 'Cancel' : '+ Note'}
        </div>
      </div>
    );
  }
}

export default Nav;
