import React, { Component } from 'react';

// Components
import NoteCard from './NoteCard';

class List extends Component {

  componentWillMount() {
    this.props.getNotes();
  }

  render() {
    const { notes, getNote, deleteNote } = this.props;

    return (
      <div className="list-container">
        {notes.map((note, index) =>
          <NoteCard
            key={index}
            index={index}
            note={note}
            getNote={getNote}
            deleteNote={deleteNote}
          />
        )}
      </div>
    );
  }
}

export default List;
